<?php

/* master.html.twig */
class __TwigTemplate_87a86df006dd2202e643b264156f3e573238b8b43873d898258852522591c01a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headextra' => array($this, 'block_headextra'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>
        <meta charset=\"UTF-8\">
        <title>";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 9
        $this->displayBlock('headextra', $context, $blocks);
        // line 10
        echo "       
         
    </head>
    <body>
        <div id=\"centeredContent\">

        ";
        // line 16
        $this->displayBlock('content', $context, $blocks);
        // line 17
        echo "

        </div>
    </body>
</html>";
    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        echo " Default ";
    }

    // line 9
    public function block_headextra($context, array $blocks = array())
    {
    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  65 => 16,  60 => 9,  54 => 8,  46 => 17,  44 => 16,  36 => 10,  34 => 9,  30 => 8,  22 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# empty Twig template #}
<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>
        <meta charset=\"UTF-8\">
        <title>{% block title %} Default {% endblock %}</title>
        {% block headextra %}{% endblock %}
       
         
    </head>
    <body>
        <div id=\"centeredContent\">

        {% block content %}{% endblock %}


        </div>
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\ipd\\slimshop\\templates\\master.html.twig");
    }
}
