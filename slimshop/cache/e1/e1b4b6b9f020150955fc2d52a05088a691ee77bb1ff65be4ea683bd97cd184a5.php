<?php

/* register.html.twig */
class __TwigTemplate_bad9f3f9ef7bb6cc18cfd77633e467e37a8c076747f1d37693fa4259450ab0c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 3);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headextra' => array($this, 'block_headextra'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo " Register";
    }

    // line 9
    public function block_headextra($context, array $blocks = array())
    {
        // line 10
        echo "        <script>
            \$(document).ready(function () {
                \$('input[name=email]').bind('propertychange change click keyup input paste',function () {
                    // AJAX request
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load('/isemailregistered/' + email);
                });
            });
        </script>
";
    }

    // line 23
    public function block_content($context, array $blocks = array())
    {
        // line 24
        echo "
            ";
        // line 25
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 26
            echo "                <ul>
                    ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 28
                echo "                        <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "                </ul>
            ";
        }
        // line 32
        echo "
            <form method=\"post\">
                Name: <input type=\"text\" name=\"name\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", array()), "html", null, true);
        echo "\"><br>
                Email: <input type=\"email\" name=\"email\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", array()), "html", null, true);
        echo "\"><span id=\"isTaken\"></span><br>
                Password: <input type=\"password\" name=\"pass1\"  ><br>
                Password:(repeated) <input type=\"password\" name=\"pass2\" ><br>

                <input type=\"submit\" value=\"Register\">
            </form>

            <p>You have Account   <a href=\"/login.html.twig\">click to Login</a></p>     


";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 35,  84 => 34,  80 => 32,  76 => 30,  67 => 28,  63 => 27,  60 => 26,  58 => 25,  55 => 24,  52 => 23,  39 => 10,  36 => 9,  30 => 5,  11 => 3,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# empty Twig template #}

{% extends \"master.html.twig\"  %}

{% block title %} Register{% endblock %}



{% block headextra %}
        <script>
            \$(document).ready(function () {
                \$('input[name=email]').bind('propertychange change click keyup input paste',function () {
                    // AJAX request
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load('/isemailregistered/' + email);
                });
            });
        </script>
{% endblock headextra %}
 


{% block content %}

            {% if errorList %}
                <ul>
                    {% for error in errorList %}
                        <li>{{error}}</li>
                        {% endfor %}
                </ul>
            {% endif %}

            <form method=\"post\">
                Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br>
                Email: <input type=\"email\" name=\"email\" value=\"{{v.email}}\"><span id=\"isTaken\"></span><br>
                Password: <input type=\"password\" name=\"pass1\"  ><br>
                Password:(repeated) <input type=\"password\" name=\"pass2\" ><br>

                <input type=\"submit\" value=\"Register\">
            </form>

            <p>You have Account   <a href=\"/login.html.twig\">click to Login</a></p>     


{% endblock content %}", "register.html.twig", "C:\\xampp\\htdocs\\ipd\\slimshop\\templates\\register.html.twig");
    }
}
