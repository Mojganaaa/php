<?php

/* addweather.html.twig */
class __TwigTemplate_cd85c0f3ae565a1d14a4001541138aff0accbaaad664c46b21332b72906cb68c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        echo "

";
        // line 9
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 10
            echo "    <ul>
        ";
            // line 11
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 12
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 14
            echo "    </ul>
";
        }
        // line 16
        echo "
<form method=\"post\">
    City: <input type=\"text\" name=\"city\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "city", array()), "html", null, true);
        echo "\"><br>
    Temperature: <input type=\"number\" name=\"tempCel\"  value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "tempCel", array()), "html", null, true);
        echo "\"><br>
    Date of reading: <input type=\"date\" name=\"readOn\"  value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "readOn", array()), "html", null, true);
        echo "\"><br>

    <input type=\"submit\" value=\"Register\">
</form>
    
    ";
    }

    public function getTemplateName()
    {
        return "addweather.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 20,  53 => 19,  49 => 18,  45 => 16,  41 => 14,  32 => 12,  28 => 11,  25 => 10,  23 => 9,  19 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# 
empty Twig template
- city name is between 2 and 50 characters long
- temperature (in Celsius) is between -100 and 100 (inclusive)
- date of reading is a valid date
#}


{% if errorList %}
    <ul>
        {% for error in errorList %}
            <li>{{error}}</li>
        {% endfor %}
    </ul>
{% endif %}

<form method=\"post\">
    City: <input type=\"text\" name=\"city\" value=\"{{v.city}}\"><br>
    Temperature: <input type=\"number\" name=\"tempCel\"  value=\"{{v.tempCel}}\"><br>
    Date of reading: <input type=\"date\" name=\"readOn\"  value=\"{{v.readOn}}\"><br>

    <input type=\"submit\" value=\"Register\">
</form>
    
    ", "addweather.html.twig", "C:\\xampp\\htdocs\\ipd\\slimfirst\\templates\\addweather.html.twig");
    }
}
