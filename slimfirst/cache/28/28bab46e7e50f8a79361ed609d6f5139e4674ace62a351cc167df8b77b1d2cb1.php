<?php

/* addweather_success.html.twig */
class __TwigTemplate_b7fcaac4260155b01838a86e85d9e4e39060366f2733c89bc349bbdb8762ffa1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<p>Weather added  : ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["city"]) ? $context["city"] : null), "html", null, true);
        echo " is ";
        echo twig_escape_filter($this->env, (isset($context["tempCel"]) ? $context["tempCel"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["readOn"]) ? $context["readOn"] : null), "html", null, true);
        echo " </p>

";
    }

    public function getTemplateName()
    {
        return "addweather_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 3,  19 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# empty Twig template #}

<p>Weather added  : {{city}} is {{tempCel}} {{readOn}} </p>

", "addweather_success.html.twig", "C:\\xampp\\htdocs\\ipd\\slimfirst\\templates\\addweather_success.html.twig");
    }
}
