<?php

require_once 'vendor/autoload.php';



//-----------------------MEEKRO.com-----------------------------
DB::$dbName = 'people';
DB::$user = 'people';
DB::$encoding = 'utf8';
DB::$password = 'i7pHv3SZ6cjKZ6Oi';


//-----------------------SLIM\Views\Twig-----------------------------
//got from http://docs.slimframework.com
// $app->get  : it means this app get ...
//('/hello/:name') : it means the URL 
//
// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');




$app->get('/hello/:name', function ($name) {
    echo "Hello, " . $name;
});



//-----------------------it inserts new data into database( myPHPAdmin)-----------------------------
$app->get('/hello/:name/:age', function ($name, $age) use ($app) {
    DB::insert('persons', array('name' => $name, 'age' => $age));

    //render the template(hello.html.twig)
    $app->render('hello.html.twig', array('name' => $name, 'age' => $age));
});



//-----------------------addperson.html( myPHPAdmin)-----------------------------

$app->get('/addperson', function() use($app) {
    //1-first show
    $app->render('addperson.html.twig');
});

$app->post('/addperson', function() use ($app) {
    // receiving a submission
    
    //$app->request() : it means request user
    $name = $app->request()->post('name');
    $age = $app->request()->post('age');
    $values = array('name' => $name, 'age' => $age);
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorList, "Name must be between 2 and 50 characters long");
        $values['name'] = '';
    }
    if (empty($age) || $age < 0 || $age > 150) {
        array_push($errorList, "Age must be between 1 and 150");
        $values['age'] = '';
    }
    if ($errorList) {
        // 3. failed submission
        $app->render('addperson.html.twig', array(
            'errorList' => $errorList,
            'v' => $values));
    } else {
        // 2. successful submission
        DB::insert('persons', array('name' => $name, 'age' => $age));
        $app->render('addperson_success.html.twig', array('name' => $name, 'age' => $age));
    }
});




//-----------------------addweather.html( myPHPAdmin)-----------------------------
$app->get('/addweather', function() use($app) {
    //1-first show
    $app->render('addweather.html.twig');
});

$app->post('/addweather', function() use ($app) {
    // receiving a submission
    
    //$app->request() : it means request user
    $city = $app->request()->post('city');
    $tempCel = $app->request()->post('tempCel');
    $readOn = $app->request()->post('readOn');
    $values = array('city' => $city, 'tempCel' => $tempCel, 'readOn' => $readOn);
    $errorList = array();
    if (strlen($city) < 2 || strlen($city) > 50) {
        array_push($errorList, "Name must be between 2 and 50 characters long");
        $values['city'] = '';
    }
    if (empty($tempCel) || $tempCel < 0 || $tempCel > 150) {
        array_push($errorList, "Age must be between 1 and 150");
        $values['tempCel'] = '';
    }
       if (empty($readOn) || $readOn < 0 || $readOn > 150) {
        array_push($errorList, "Age must be between 1 and 150");
        $values['readOn'] = '';
    }
    if ($errorList) {
        // 3. failed submission
        $app->render('addweather.html.twig', array(
            'errorList' => $errorList,
            'v' => $values));
    } 
    
    else {
        // 2. successful submission
        DB::insert('weathers', array('city' => $city, 'tempCel' => $tempCel));
        $app->render('addweather_success.html.twig', array('city' => $city, 'tempCel' => $tempCel, 'readOn' => $readOn ));
    }
});


$app->run();