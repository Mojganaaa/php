<?php require_once 'db.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="styles.css">
        <title>Article view</title>        
    </head>
    <body>
    <div id="centeredContent">
<?php

if (!isset($_GET['id'])) {
    die("Error: id must be provided as script parameter");
}

$id = $_GET['id'];

$sql = "SELECT username, creationTime, title, body FROM article,users WHERE "
        . "article.authorId=users.id AND article.id=" . mysqli_real_escape_string($link, $id);
$result = mysqli_query($link, $sql);
if (!$result) {
    die("SQL query error: " . mysqli_error($link));
}

$row = mysqli_fetch_assoc($result);
if ($row) {
    // print_r($row);
    printf('<div class="articleContainer">'
        . '<p class="title">%s by %s create on %s<p>'
            . '<div class="articleBody">%s</div></div>',
            $row['title'], $row['username'], $row['creationTime'], $row['body']);
} else {
    printf("<p>Article with ID=%s not found", $id);
}

?>
    </div>
    </body>
</html>
 
