<?php

require_once 'db.php';

// only allow logged in users past this point
if (!$_SESSION['user']) {
    die("<p>Authorized users only. You must <a href=login.php>login</a> to access this page.</p>");
}
$userId = $_SESSION['user']['id'];

function printForm($values = array('title' => '', 'body' => '')) {
    // here-doc
    $t = $values['title'];
    $b = $values['body'];
    $form = <<< ROSESAREBEST
<form method="post">
    Title: <input type="text" name="title" value="$t"><br>
    <textarea name="body">$b</textarea><br>
    <input type="submit" value="Add article">
</form>
ROSESAREBEST;
    echo $form;
}

if (isset($_POST['title'])) {
    // extract submission
    $title = $_POST['title'];
    $body = $_POST['body'];
    $values = $_POST;
    //
    $errorList = array();
    if (strlen($title) < 10 || (strlen($title) > 100)) {
        array_push($errorList, "Title must be between 10 and 100 characters long");
        // $values['title'] = "";
    }
    if (strlen($body) < 10 || (strlen($body) > 4000)) {
        array_push($errorList, "Body must be between 10 and 100 characters long");
        // $values['title'] = "";
    }
    // array with 1 or more elements is considered "True" value
    if ($errorList) {
        // errors - failed submission
        echo "<p>Your submission has problems:</p>\n";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>\n";
        }
        echo "</ul>\n";
        printForm($values);
    } else {
        // successful submission
        // FIXME: SQL injection possible here !!! CYA policy applies
        $sql = sprintf("INSERT INTO article VALUES (NULL, '%s', NULL, '%s', '%s')",
                $userId, 
                mysqli_real_escape_string($link, $title),
                mysqli_real_escape_string($link, $body));
        $result = mysqli_query($link, $sql);
        if (!$result) {
            die("SQL query error: " . mysqli_error($link));
        }
        $id = mysqli_insert_id($link);
        echo "<p>Article has been posted. <a href=\"article.php?id=$id\">Click here to view</a></p>\n";
    }
} else {
    // STATE 1: first show
    printForm();
}
