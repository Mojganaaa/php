Day 04 homework
===============

Create project 'climate' datatabase 'climate' with user 'climate'.

Create table 'users' with columns:
* id INT PK AI
* email VC(250)
* password VC(100)

Create table 'cities' with columns:
* id INT PK AI
* name VC(100)
* latitude DECIMAL(10, 8)
* longitude DECIMAL(11, 8) NOT NULL

Create table 'weathers' with columns:
* id INT PK AI
* cityId INT FK->cities.id
* onDate DATE
* temperatureCelsius float
* windKph float
* windDirection ENUM('N','NE','E','SE','S','SW','W','NW')

Create the following php files.
* db.php
* register.php
* login.php
* logout.php
* citylist.php
* cityadd.php
* cityweatherview.php?cityid=123
* cityweatheradd.php?cityid=123


db.php
------

Standard db.php as used before, including session_start();


register.php
------------

Standard register.php as used before, including AJAX use to make sure email is not registered yet.
Passwords must be encrypted.

login.php
---------

Standard login.php as used before. Email is the username.
Make sure you remember logged in user in session.

logout.php
----------
Standard logout.php as used before.


citylist.php
------------

Displays a table of all cities, like in the database.

Last, additional column "Actions" of the table contains two links
* "view weather reports" which links to that citie's weather reports using  cityweatherview.php?cityid=123 URL.
* "add weather report" which links to that citie's weather reports add form using cityweatheradd.php?cityid=123 URL.

Similar to:
http://www.binaryintellect.net/articles/content/Images/T_EditUpdateSameView_01.png

Under the table there's a link to add new city linking to cityadd.php.


cityadd.php
-----------

Can only be accessed by a logged-in user.

3-state form for adding new city to the list. Using POST. Verify that city name is 2-100 characers long.
Verify that longitude and latitude are within required ranges as per their definition.


cityweatherview.php?cityid=123
------------------------------

Shows a table with all weather reports for a given city, ordered from newest to oldest by 'onDate' field.


cityweatheradd.php?cityid=123
-----------------------------

Can only be accessed by a logged-in user.

3-state form for adding weather for a particular city to the list of weathers. Using POST.
Verify that:
* onDate is a valid date
* temperatureCelsius float is between -100 and 100 and not empty
* windKph float is between 0 and 500.
