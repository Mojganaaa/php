

<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <meta charset="UTF-8">
        <title></title>
        <script>
            $(document).ready(function() {
                $('input[name=email]').keyup(function() {
                    // AJAX request
                    var username = $('input[name=email]').val();
                    $('#isTaken').load('isusernametaken.php?email=' + username);
                });
            });
        </script>
    </head>
    <body>
        <div id="centeredContent">
            <?php
            require_once 'db.php';

// for debuggin only
// print_r($_POST);

            function printForm($values = array( 'email' => '')) {
                // here-doc
                $e = $values['email'];
                $form = <<< ROSESAREBEST
<form method="post">
 
    Email: <input type="text" name="email" value="$e"><span id="isTaken"></span><br>
    Password: <input type="password" name="pass1"><br>
    Password (repated): <input type="password" name="pass2"><br>
   
    <input type="submit" value="Register">
</form>
ROSESAREBEST;
                echo $form;
            }

            if (isset($_POST['email'])) {
                // extract submission
                $email = $_POST['email'];
                $pass1 = $_POST['pass1'];
                $pass2 = $_POST['pass2'];
                
                $values = $_POST;
                //
                $errorList = array();
//                if (!preg_match('/^[A-Za-z0-9_]{5,20}$/', $username)) {
//                    array_push($errorList, "Name must be between 2 and 50 characters long"
//                            . "and consist of upper/lower case characters digits and underscore only.");
//                    $values['email'] = "";
//                } else { // username seems valid - check if it is available
//                    $sql = sprintf("SELECT * FROM users WHERE email='%s'",
//                            mysqli_real_escape_string($link, $username));
//                    $result = mysqli_query($link, $sql);
//                    if (!$result) {
//                        die("SQL query error: " . mysqli_error($link));
//                    }
                    $row = mysqli_fetch_assoc($result);
                    if ($row) {
                        array_push($errorList, "Email already taken, choose a different one.");
                        $values['email'] = "";
                    }
                
                if ($pass1 != $pass2) {
                    array_push($errorList, "Passwords do not match");
                } else {
                    if (strlen($pass1) < 6 || strlen($pass1) > 100) {
                        array_push($errorList, "Passwords too short/long, must be between 6 and 100 characters");
                    }// \ + * ? [ ^ ] $ ( ) { } = ! < > | : -
                    if (!preg_match('/[A-Z]/', $pass1) || !preg_match('/[a-z]/', $pass1) || !preg_match('/[0-9' . preg_quote("!@#\$%^&*()_-+={}[],.<>;:'\"~`") . ']/', $pass1)) {
                        array_push($errorList, "Password must include at least one character in each three categories: "
                                . "uppercase letter, lowercase letter, digit or special character");
                    }
                }
                if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
                    array_push($errorList, "Email seems invalid");
                }
                // array with 1 or more elements is considered "True" value
                if ($errorList) {
                    // errors - failed submission
                    echo "<p>Your submission has problems:</p>\n";
                    echo "<ul>\n";
                    foreach ($errorList as $error) {
                        echo "<li>$error</li>\n";
                    }
                    echo "</ul>\n";
                    printForm($values);
                } else {
                    // successful submission
                    // FIXME: SQL injection possible here !!! CYA policy applies
                    $passEnc = password_hash($pass1, PASSWORD_BCRYPT);
                    $sql = sprintf("INSERT INTO users VALUES (NULL, '%s', '%s')",  mysqli_real_escape_string($link, $email), mysqli_real_escape_string($link, $passEnc));
                    $result = mysqli_query($link, $sql);
                    if (!$result) {
                        die("SQL query error: " . mysqli_error($link));
                    }
                    echo "<p>You've been added:  $email</p>\n";
                }
            } else {
                // STATE 1: first show
                printForm();
            }
            ?>

        </div>
    </body>
</html>