<?php

require_once 'db.php';

// only allow logged in users past this point
if (!$_SESSION['user']) {
    die("<p>Authorized users only. You must <a href=login.php>login</a> to access this page.</p>");
}
$userId = $_SESSION['user']['id'];





function printForm($values = array('name' => '', 'latitude' => '', 'longitude' => '')) {
    // here-doc
    $n = $values['name'];
    $la = $values['latitude'];
    $lo = $values['longitude'];
    $form = <<< ROSESAREBEST
<form method="post">
     City Name: <input type="text" name="name" value="$n"><br>
    Latitude: <input type="text" name="latitude" value="$la"><br>
    Longitude: <input type="text" name="longitude" value="$lo"><br>
    <input type="submit" value="Add article">
</form>
ROSESAREBEST;
    echo $form;
}

if (isset($_POST['name'])) {
    // extract submission
    $name = $_POST['name'];
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $values = $_POST;
    //
    $errorList = array();
    if (strlen($name) < 2 || (strlen($name) > 100)) {
        array_push($errorList, "Title must be between 10 and 100 characters long");
        // $values['title'] = "";
    }
    if (strlen($latitude) < 1 || (strlen($latitude) > 100)) {
        array_push($errorList, "Body must be between 10 and 100 characters long");
        // $values['title'] = "";
    }
     if (strlen($longitude) < 1 || (strlen($longitude) > 100)) {
        array_push($errorList, "Body must be between 10 and 100 characters long");
        // $values['title'] = "";
    }
    // array with 1 or more elements is considered "True" value
    if ($errorList) {
        // errors - failed submission
        echo "<p>Your submission has problems:</p>\n";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>\n";
        }
        echo "</ul>\n";
        printForm($values);
    } else {
        // successful submission
        // FIXME: SQL injection possible here !!! CYA policy applies
        $sql = sprintf("INSERT INTO cities VALUES (NULL, '%s', '%s', '%s')",
                $userId, 
                mysqli_real_escape_string($link, $name),
                mysqli_real_escape_string($link, $latitude),
                mysqli_real_escape_string($link, $longitude));
        $result = mysqli_query($link, $sql);
        if (!$result) {
            die("SQL query error: " . mysqli_error($link));
        }
        $id = mysqli_insert_id($link);
        echo "<p>City has been posted. <a href=\"citylist.php?id=$id\">Click here to view</a></p>\n";
    }
} else {
    // STATE 1: first show
    printForm();
}

