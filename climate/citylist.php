<?php
require_once 'db.php';

$sql = "SELECT * FROM cities";
$result = mysqli_query($link, $sql);
if (!$result) {
    die("SQL query error: " . mysqli_error($link));
}

echo "<table border=1><tr><th>ID</th><th>City Name</th><th>Latitude</th><th>Longitude</th></tr>\n";
while ($row = mysqli_fetch_assoc($result)) {
    // print_r($row);
    printf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n",
            $row['id'], $row['name'], $row['latitude'], $row['longitude']);
}
echo "</table>";
