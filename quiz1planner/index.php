<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
              <?php
require_once 'db.php';

       // only allow logged in users past this point
if (!$_SESSION['user']) {
    die("<p>Authorized users only. You must <a href=login.php>login</a> to access this page.</p>");
}
$userId = $_SESSION['user']['id'];
 ?>
        
        
        <p>You can Add another Trip, <a href=" addtrip.php"> click to continue</a>.</p>
       
        


  <?php
$sql = "SELECT * FROM trips";
$result = mysqli_query($link, $sql);
if (!$result) {
    die("SQL query error: " . mysqli_error($link));
}


echo "<table border=1><tr><th>ID</th><th>Traveller Id</th><th>Departure Date</th><th>From City</th><th>To City</th><th>Transportation</th></tr>\n";
while ($row = mysqli_fetch_assoc($result)) {
    // print_r($row);
    printf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n",
            $row['id'], $row['travellerId'], $row['departureDate'], $row['fromCity'], $row['toCity'], $row['transportation']); 
}
echo "</table>";

        ?>
    </body>
</html>
