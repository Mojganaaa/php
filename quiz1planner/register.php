<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <meta charset="UTF-8">
      
        <title></title>
        <script>
            $(document).ready(function() {
                $('input[name=passport]').keyup(function() {
                    // AJAX request
                    var passport = $('input[name=passport]').val();
                    $('#isTaken').load('ispassportregistered.php?passport=' + passport);
                });
            });
        </script>
    </head>
    <body>
        <div id="centeredContent">
            <?php
            require_once 'db.php';

// for debuggin only
// print_r($_POST);

            function printForm($values = array('passport' => '')) {
                // here-doc
                $pp = $values['passport'];
              
                $form = <<< ROSESAREBEST
<form  class="form" method="post">
   Passport Number: <input type="text" name="passport" value="$pp"><span id="isTaken"></span><br>
    Password: <input type="password" name="pass1"><br>
    Password (repeated): <input type="password" name="pass2"><br>
  
    <input type="submit" value="Register">
</form>
ROSESAREBEST;
                echo $form;
            }

            if (isset($_POST['passport'])) {
                // extract submission
                $passport = $_POST['passport'];
                $pass1 = $_POST['pass1'];
                $pass2 = $_POST['pass2'];
         
                $values = $_POST;
                //
                $errorList = array();
                if (!preg_match('/^([A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9])*$/', $passport)) {
                    array_push($errorList, "Passport Number should be valid." );
     
                    $values['passport'] = "";
                } else { // passport seems valid - check if it is available
                    $sql = sprintf("SELECT * FROM travellers WHERE passport='%s'",
                            mysqli_real_escape_string($link, $passport));
                    $result = mysqli_query($link, $sql);
                    if (!$result) {
                        die("SQL query error: " . mysqli_error($link));
                    }
                    $row = mysqli_fetch_assoc($result);
                    if ($row) {
                        array_push($errorList, "Passport Number is already Entered, choose a different one.");
                        $values['passport'] = "";
                    }
                }
                if ($pass1 != $pass2) {
                    array_push($errorList, "Passwords do not match");
                } else {
                    if (strlen($pass1) < 6 || strlen($pass1) > 100) {
                        array_push($errorList, "Passwords too short/long, must be between 6 and 100 characters");
                    }// \ + * ? [ ^ ] $ ( ) { } = ! < > | : -
                    if (!preg_match('/[A-Z]/', $pass1) || !preg_match('/[a-z]/', $pass1) || !preg_match('/[0-9' . preg_quote("!@#\$%^&*()_-+={}[],.<>;:'\"~`") . ']/', $pass1)) {
                        array_push($errorList, "Password must include at least one character in each three categories: "
                                . "uppercase letter, lowercase letter, digit or special character");
                    }
                }
               
                // array with 1 or more elements is considered "True" value
                if ($errorList) {
                    // errors - failed submission
                    echo "<p>Your submission has problems:</p>\n";
                    echo "<ul>\n";
                    foreach ($errorList as $error) {
                        echo "<li>$error</li>\n";
                    }
                    echo "</ul>\n";
                    printForm($values);
                } else {
                    // successful submission
                    // FIXME: SQL injection possible here !!! CYA policy applies
                    $passEnc = password_hash($pass1, PASSWORD_BCRYPT);
                    $sql = sprintf("INSERT INTO travellers VALUES (NULL, '%s', '%s')", mysqli_real_escape_string($link, $passport), mysqli_real_escape_string($link, $passEnc));
                    $result = mysqli_query($link, $sql);
                    if (!$result) {
                        die("SQL query error: " . mysqli_error($link));
                    }
                    echo "<p>You've been Logged in with This Passport Number: $passport </p>\n";
                }
            } else {
                // STATE 1: first show
                printForm();
            }
            ?>

        </div>
    </body>
</html>