<?php

require_once 'db.php';

//// only allow logged in users past this point
if (!$_SESSION['user']) {
    die("<p>Authorized users only. You must <a href=login.php>login</a> to access this page.</p>");
}
$userId = $_SESSION['user']['id'];



function printForm($values = array('travellerId' => '','departureDate' => '', 'fromCity' => '','toCity' => '','transportation' => '')) {
    // here-doc
    $ti = $values['travellerId'];
    $dd = $values['departureDate'];
    $fc = $values['fromCity'];
    $tc = $values['toCity'];
  $t = $values['transportation'];
    
    $form = <<< ROSESAREBEST
<form class="form" method="post">
    Departure Date: <input type="text" name="departureDate" value="$dd"><br>
    From City: <input type="text" name="fromCity" value="$fc"><br>
    To City: <input type="text" name="toCity" value="$tc"><br>
    Transportation: 
   <select>
  <option type="text" name="transportation" value="$t">car</option>
  <option type="text" name="transportation" value="$t">bus</option>
  <option type="text" name="transportation"  value="$t">train</option>
  <option type="text" name="transportation" value="$t">plane</option>
    <option type="text" name="transportation" value="$t">bike</option>
  <option type="text" name="transportation" value="$t">boat</option>
    <option type="text" name="transportation" value="$t">other</option>
</select>     
   <br>
    <input type="submit" value="Add Trip">
</form>
            

            
            
ROSESAREBEST;
    echo $form;
}

if (isset($_POST['travellerId'])) {
    // extract submission
     $ti = $_POST['travellerId'];
    $dd = $_POST['departureDate'];
     $fc = $_POST['fromCity'];
     $tc = $_POST['toCity'];
    $t = $_POST['transportation'];
    $values = $_POST;
    //
    $errorList = array();
    
    
    if (strlen($dd) < 1 || (strlen($dd) > 10)) {
        array_push($errorList, "DepartureDate must be set");
        // $values['title'] = "";
    }
    if (strlen($fc) < 5 || (strlen($fc) >50)) {
        array_push($errorList, "city must be between 5 and 50 characters long");
        // $values['fromCity'] = "";
    }
     if (strlen($tc) < 5 || (strlen($tc) >50)) {
        array_push($errorList, "city must be between 5 and 50 characters long");
        // $values['toCity'] = "";
    }
      if (strlen($t) < 1 || (strlen($t) >50)) {
        array_push($errorList, "transportation must be between 5 and 50 characters long");
        // $values['toCity'] = "";
    }
    // array with 1 or more elements is considered "True" value
    if ($errorList) {
        // errors - failed submission
        echo "<p>Your submission has problems:</p>\n";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>\n";
        }
        echo "</ul>\n";
        printForm($values);
    } else {
        // successful submission
        // FIXME: SQL injection possible here !!! CYA policy applies
        $sql = sprintf("INSERT INTO trips VALUES (NULL, '%s','%s','%s', '%s','%s')",
                $userId,
                mysqli_real_escape_string($link, $ti),
                mysqli_real_escape_string($link, $dd),
                mysqli_real_escape_string($link, $fc),
                mysqli_real_escape_string($link, $tc),
          mysqli_real_escape_string($link, $t));
        $result = mysqli_query($link, $sql);
        if (!$result) {
            die("SQL query error: " . mysqli_error($link));
        }
        $id = mysqli_insert_id($link);
        echo "<p>Trip has been posted. <a href=\"index.php?id=$id\">Click here to view</a></p>\n";
    }
} else {
    // STATE 1: first show
    printForm();
}
