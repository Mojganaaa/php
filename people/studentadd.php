<?php
require_once 'db.php';
// for debuggin only
// print_r($_GET);

function printForm($values = array('name' => '', 'gpa' => '' , 'year' => '')) {
    // here-doc
    $n = $values['name'];
    $g = $values['gpa'];
    $y =$values['year'];
    $form = <<< ROSESAREBEST
<form>
    Name: <input type="text" name="name" value="$n"><br>
    GPA: <input type="number" step="0.01" name="gpa" value="$g"><br>
    Birth Year: <input type="number" name="year" value="$y"><br>
    <input type="submit" value="Add student">
</form>
ROSESAREBEST;
    echo $form;
}

if (isset($_GET['name'])) {
    // extract submission
    $name = $_GET['name'];
    $gpa = $_GET['gpa'];
    $year = $_GET['year'];
    $values = $_GET;
    //
    $errorList = array();
    if (strlen($name) < 2 || (strlen($name) > 50)) {
        array_push($errorList, "Name must be between 2 and 50 characters long");
        $values['name'] = "";
    }
    if ($gpa == ''|| $gpa < 0 || $gpa > 4.3) {
        array_push($errorList, "GPA must be between 0 and 4.3");
        $values['gpa'] = "";
    }
     if ($year < 1900 || $year > 2100) {
        array_push($errorList, "year of birth must be between1900 and 2100");
        $values['year'] = "";
    }
    // array with 1 or more elements is considered "True" value
    if ($errorList) {
        // errors - failed submission
        echo "<p>Your submission has problems:</p>\n";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>\n";
        }
        echo "</ul>\n";
        printForm($values);
    } else { 
     // successful submission--------------------------------------
        
        
        
        //sql
         //SQL Injection --------------------------------------
        $sql= sprintf(
        
                
        //table name is students:
        // '%s'  we use this for string         
        //  %s   we use this for numbers     
                
        "insert into students VALUES ( NULL , '%s', %s, %s)",
        mysqli_real_escape_string($link, $name),
        mysqli_real_escape_string($link, $gpa),
        mysqli_real_escape_string($link, $year)
                
                );  
        
        
        $result = mysqli_query($link, $sql);
                
         if (!$result){
             
            die ("SQL query error :"  . mysqli_error($link));
             
         }       
               
        echo "<p>You've been added: Name is $name and the Birth of year is  $year and your GPA is $gpa </p>\n";
    }
} 
else {
    // STATE 1: first show
    printForm();
}
