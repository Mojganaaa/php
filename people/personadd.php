<?php
require_once 'db.php';


// for debuggin only
// print_r($_GET);

function printForm($values = array('name' => '', 'age' => '')) {
    // here-doc
    $n = $values['name'];
    $a = $values['age'];
    $form = <<< ROSESAREBEST
<form>
    Name: <input type="text" name="name" value="$n"><br>
    Age: <input type="number" name="age" value="$a"><br>
    <input type="submit" value="Add person">
</form>
ROSESAREBEST;
    echo $form;
}

if (isset($_GET['name'])) {
    // extract submission
    $name = $_GET['name'];
    $age = $_GET['age'];
    $values = $_GET;
    //
    $errorList = array();
    if (strlen($name) < 2 || (strlen($name) > 50)) {
        array_push($errorList, "Name must be between 2 and 50 characters long");
        $values['name'] = "";
    }
    if ($age < 1 || $age > 150) {
        array_push($errorList, "Age must be between 1 and 150");
        $values['age'] = "";
    }
    // array with 1 or more elements is considered "True" value
    if ($errorList) {
        // errors - failed submission
        echo "<p>Your submission has problems:</p>\n";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>\n";
        }
        echo "</ul>\n";
        printForm($values);
    } else {
     
        // successful submission
       //--------------------------------------------------------  
     
        
        //SQL Injection --------------------------------------
        $sql= sprintf(
        
                
        //table name is persons:        
        "insert into persons VALUES ( NULL , '%s', %s)",
        mysqli_real_escape_string($link, $name),
        mysqli_real_escape_string($link, $age)
                
                );  
        
        
        $result = mysqli_query($link, $sql);
                
         if (!$result){
             
            die ("SQL query error :"  . mysqli_error($link));
             
         }       
                
        echo "<p>You've been added: Name $name and $age</p>\n";
    }
} else {
    // STATE 1: first show
    printForm();
}
